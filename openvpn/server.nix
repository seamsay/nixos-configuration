{ config, lib, pkgs, ... }:
let isServer = config.openvpn.purpose == "server";
in lib.mkIf isServer {
  security.sudo.extraRules = [{
    commands = [{
      command = "${pkgs.openvpn}/bin/openvpn";
      options = [ "NOPASSWD" ];
    }];
    users = [ "openvpn-server" ];
  }];

  # TODO: Write automated script to setup CA and server.
  users.users = {
    openvpn-ca = {
      description = "OpenVPN Certificate Authority";
      isNormalUser = true;
      packages = with pkgs; [ easyrsa openvpn ];
    };
    openvpn-server = {
      description = "OpenVPN Server";
      isNormalUser = true;
      packages = with pkgs; [
        easyrsa
        openvpn
        # TODO: Run openvpn as openvpn-server (didn't work last time, maybe because we used .../bin/ip instead of .../sbin/ip?).
        (let
          config = writeText "openvpn-server.conf" ''
            port 1194

            proto udp

            dev tun

            ca /home/openvpn-server/pki/ca.crt
            cert /home/openvpn-server/pki/issued/openvpn-server.crt
            key /home/openvpn-server/pki/private/openvpn-server.key

            dh /home/openvpn-server/pki/dh.pem

            topology subnet

            server 10.8.0.0 255.255.255.0

            ifconfig-pool-persist /home/openvpn-server/ifconfig-pool-persist.txt

            push "redirect-gateway def1 bypass-dhcp"

            keepalive 10 120

            tls-auth /home/openvpn-server/pki/private/ta.key 0

            cipher AES-256-CBC

            compress lz4-v2
            push "compress lz4-v2"

            group nobody
            user nobody

            persist-key
            persist-tun

            status /home/openvpn-server/openvpn-status.log
            log-append /home/openvpn-server/openvpn-server.log

            verb 3

            mute 20

            explicit-exit-notify 1
          '';
        in writeShellScriptBin "run-openvpn" ''
          sudo ${openvpn}/bin/openvpn --config ${config}
        '')
      ];
    };
  };
}
