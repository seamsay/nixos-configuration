{ lib, ... }: {
  options.openvpn.purpose = lib.mkOption {
    type = lib.types.nullOr (lib.types.enum [ "server" "client" ]);
    default = null;
    description = ''
      How this machine interacts with OpenVPN.
      <variablelist>
        <varlistentry>
          <term><literal>null</literal></term>
          <listitem><para>Does not use OpenVPN at all.</para></listitem>
        </varlistentry>
        <varlistentry>
          <term><literal>server</literal></term>
          <listitem><para>Provides an OpenVPN server.</para></listitem>
        </varlistentry>
        <varlistentry>
          <term><literal>client</literal></term>
          <listitem><para>Connects to an OpenVPN server.</para></listitem>
        </varlistentry>
      </variablelist>
    '';
  };

  imports = [ ./server.nix ];
}
