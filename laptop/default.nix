{ config, pkgs, ... }: {
  boot.extraModulePackages = [ config.boot.kernelPackages.acpi_call ];
  services.tlp.enable = true;

  systemd.user = {
    services = {
      battery-notify = {
        description = "Send the user a notification if the battery is low.";
        script = with pkgs;
          let
            battery-notifier = writeTextFile {
              name = "battery-notifier";
              executable = true;
              # TODO: Put this in separate file?
              text = ''
                #!${bash}/bin/bash

                set -eu
                set -o pipefail

                notify() {
                    percentage="$1"
                    urgency="$2"
                    notified="$3"

                    if [ "$(${coreutils}/bin/cat "$notified")" = "$urgency" ]
                    then
                        2>&1 echo "INFO: Already sent a $urgency notfication."
                    else
                        ${libnotify}/bin/notify-send -u "$urgency" "Battery level: $percentage%"
                        echo "$urgency" >"$notified"
                    fi
                }

                percentage='0'
                number='0'
                discharging=

                for supply in /sys/class/power_supply/*
                do
                    [ "$(${coreutils}/bin/cat "$supply/type")" = 'Battery' ] || continue

                    now="$(${coreutils}/bin/cat "$supply/energy_now")"
                    full="$(${coreutils}/bin/cat "$supply/energy_full")"

                    percentage="$percentage + ($now/$full)"
                    number="$number + 1"

                    if [ "$(${coreutils}/bin/cat "$supply/status")" = 'Discharging' ]
                    then
                        discharging='1'
                    fi
                done

                notified="''${XDG_RUNTIME_DIR:-/run/user/$(id -u "$USER")}/battery-notifier"
                ${coreutils}/bin/touch "$notified"

                percentage="$(echo "scale=2; 100 * ($percentage)/($number)" | ${bc}/bin/bc)"
                2>&1 echo "INFO: Battery at $percentage%."

                if [ -z "''${discharging:-}" ]
                then
                    2>&1 echo 'INFO: No batteries discharging, not sending notifications.'
                    ${coreutils}/bin/rm -f "$notified"
                elif [ "$(echo "$percentage < 10" | ${bc}/bin/bc)" -eq 1 ]
                then
                    notify "$percentage" 'critical' "$notified"
                elif [ "$(echo "$percentage < 20" | ${bc}/bin/bc)" -eq 1 ]
                then
                    notify "$percentage" 'normal' "$notified"
                elif [ "$(echo "$percentage < 30" | ${bc}/bin/bc)" -eq 1 ]
                then
                    notify "$percentage" 'low' "$notified"
                else
                    2>&1 echo 'INFO: Battery not low, not sending notifications.'
                    ${coreutils}/bin/rm -f "$notified"
                fi
              '';
              checkPhase = "${shellcheck}/bin/shellcheck $out";
            };
          in "${battery-notifier}";
      };
    };

    timers = {
      battery-notify = {
        description = "Run battery notification service regularly.";

        wantedBy = [ "timers.target" ];

        timerConfig = {
          OnCalendar = "minutely";
          Unit = "battery-notify.service";
        };
      };
    };
  };
}
