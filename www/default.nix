{ config, lib, pkgs, ... }:
let
  directory = config.users.users.website.home;
  port = 1111;
in {
  networking.firewall.allowedTCPPorts = lib.lists.unique [
    port
    # For certbot to work.
    80
  ];

  systemd = let database = "${directory}/sqlite.db";
  in {
    services.website = let
      pname = "personal-server";
      version = "0.1.0";
      src = pkgs.fetchFromGitLab {
        owner = "seamsay";
        repo = pname;
        rev = "18888fc003cd0b6b58bf599dd4474b14e42de0d7";
        hash = "sha256:1xdvdwq084bxvmiwsfidhyxd6hwc6vk512lc9qypg7nvnfz94yi0";
      };
      server = pkgs.rustPlatform.buildRustPackage {
        pname = pname;
        version = version;

        src = "${src}/server";
        cargoHash =
          "sha256:1qm0ndzirihzc64j0ijfmm269d6bq7wcrypipfnfmpplfl3z7y0d";

        buildInputs = [ pkgs.sqlite ];

        # Enable nightly features in stable.
        RUSTC_BOOTSTRAP = 1;
      };
    in {
      description = "Personal Website";

      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];

      serviceConfig = {
        User = "website";
        WorkingDirectory = config.users.users.website.home;
      };

      preStart = ''
        if [ ! -e '${database}' ]
        then
          ${pkgs.sqlite}/bin/sqlite3 '${database}' <'${src}/db/schema-sqlite.sql'
        elif [ ! -f '${database}' ]
        then
          >&2 echo 'Database (${database}) exists, but is not a file!'
          exit 1
        fi
      '';

      script = ''
        export ROCKET_DATABASES='{sqlite={url="${database}"}}'
        export ROCKET_PORT=${toString port}
        export ROCKET_SECRET_KEY='0Z+A6euJcr7k0fMkLtnVpE01wZZcqszaUPOiSiwtNlw='
        export ROCKET_TEMPLATE_DIR='${src}/templates'
        export ROCKET_TLS='{certs="${directory}/certs/live/www.seamsay.xyz/fullchain.pem",key="${directory}/certs/live/www.seamsay.xyz/privkey.pem"}'
        exec ${server}/bin/server
      '';
    };

    timers.renew-certificates = {
      description = "Renew Let's Encrypt Certificates Timer";

      wantedBy = [ "timers.target" ];
      after = [ "network.target" ];

      timerConfig = {
        OnCalendar = "daily";
        Unit = "renew-certificates.service";
      };
    };

    services.renew-certificates = {
      description = "Renew Let's Encrypt Certificates";

      preStart = "systemctl stop website.service";
      script =
        "${pkgs.certbot}/bin/certbot renew --config-dir '${directory}/certs' --logs-dir '${directory}/certs/logs' --work-dir '${directory}/.local/run/certbot'";
      # TODO: I feel like `chown`ing this is indicative of something wrong...
      postStop = ''
        chown -R website '${directory}/certs'
        systemctl start website.service
      '';
    };

    timers.backup-database = {
      description = "Backup SQLite Database Timer";

      wantedBy = [ "timers.target" ];

      timerConfig = {
        OnCalendar = "daily";
        Unit = "backup-database.service";
      };
    };

    services.backup-database = {
      description = "Backup SQLite Database";

      serviceConfig.User = "website";

      script = let backups = "${directory}/backup";
      in ''
        ${pkgs.sqlite}/bin/sqlite3 '${database}' <<<'.dump' | sed -e 's/\(pragma\s\+foreign_keys\s*=\s*\)off/\1ON/i' | ${pkgs.gzip}/bin/gzip >"${backups}/sqlite.db.sql.$(date +'%Y-%m-%d').gz"
        ${pkgs.python3}/bin/python3 ${
          ./rotate-backups.py
        } --directory '${backups}' --regex '(?P<id>sqlite\.db)\.sql\.(?P<date>\d{4}-\d{2}-\d{2})\.gz'
      '';
    };
  };

  users.users.website = {
    description = "Personal Website";
    isNormalUser = true;
    packages = [
      pkgs.certbot
      (let
        init = pkgs.writeText "sqliterc" ''
          .headers on
          .mode column
        '';
      in pkgs.writeShellScriptBin "sqlite"
      ''${pkgs.sqlite}/bin/sqlite3 -init '${init}' "$@"'')
    ];
  };
}
