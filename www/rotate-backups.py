#!${pkgs.python3}/bin/python3

import argparse
import datetime
import logging
import os
import re
import sys

# Last 7 days daily.
# Last 4 weeks weekly.
# Last 12 months monthly.
# Last 10 years yearly.

def find_closest_file_to_date(target, files, regex):
    closest = None
    distance = datetime.timedelta.max

    for file in files:
        current = datetime.datetime.strptime(regex.fullmatch(file).group("date"), "%Y-%m-%d").date()
        current_distance = abs(current - target)

        if current_distance < distance:
            closest = file
            distance = current_distance

    logging.info("KEEP   %s ~ %s", closest, target)
    return closest

logging.basicConfig(
    level=logging.INFO,
    format="{levelname:8} rotate-backups {message}",
    style="{",
)

argparser = argparse.ArgumentParser()
argparser.add_argument("--directory", help="directory containing backups")
argparser.add_argument("--regex", help="regex to extract the date and database name from the filename")
args = argparser.parse_args()

regex = re.compile(args.regex)

all = set(os.listdir(args.directory))
files = {
    file
    for file in all
    if (
        # Ignore any files that are not backups.
        regex.fullmatch(file) is not None and
        # Currently ignore any files that are not for SQLite.
        regex.fullmatch(file).group("id") == "sqlite.db"
    )
}
keep = set()

for file in all - files:
    logging.warning("IGNORE %s", file)

today = datetime.date.today()
keep.add(find_closest_file_to_date(today, files, regex))
for i in range(1, 7):
    date = today - datetime.timedelta(days=i)
    keep.add(find_closest_file_to_date(date, files, regex))

week = today - datetime.timedelta(days=today.weekday())
for i in range(1, 4):
    date = week - datetime.timedelta(days=7*i)
    keep.add(find_closest_file_to_date(date, files, regex))

month = today.replace(day=1)
for i in range(1, 12):
    date = month
    for j in range(i):
        date = (date - datetime.timedelta(days=1)).replace(day=1)
    keep.add(find_closest_file_to_date(date, files, regex))

year = today.replace(month=1, day=1)
for i in range(1, 10):
    date = year.replace(year=year.year - i)
    keep.add(find_closest_file_to_date(date, files, regex))

for file in files - keep:
    path = os.path.join(args.directory, file)
    logging.info("REMOVE %s (%s)", file, path)
    os.remove(path)
