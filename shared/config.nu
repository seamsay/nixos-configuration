let-env config = {
    sync_history_on_enter: false
}

mkdir ~/.cache/starship
starship init nu | save ~/.cache/starship/init.nu
source ~/.cache/starship/init.nu

# Create a directory, then change to it.
def-env make-change-directory [
    directory: path  # The directory to create.
] {
    mkdir $directory
    cd $directory
}
alias mcd = make-change-directory

# Change to a directory, then view the contents of it.
def-env change-list-directory [
    directory: path  # The directory to change to.
] {
    cd $directory
    ls
}
alias cdl = change-list-directory

