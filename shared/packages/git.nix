{ lib, pkgs, ... }: {
  environment.systemPackages = lib.singleton pkgs.git;

  home-manager.sharedModules = lib.singleton {
    programs.git = {
      enable = true;

      aliases = {
        aliases =
          "! git config --get-regexp ^alias\\. | sed -e s/^alias\\.// -e s/\\ /\\0/ | grep -v ^aliases | sort | column -ts \\0";
        add-conflicts = "! git add $(git conflicts)";
        # TODO: This should error if we're not in a merge conflict.
        conflicts = "diff --name-only --diff-filter=u";
        create = "! sh -c 'git branch $1 && git checkout $1 && git push -u'";
        edit-conflicts = "! $EDITOR $(git conflicts)";
        fixup = ''
          ! sh -c '(git diff-files --quiet || (echo "Unstaged changes, please commit or stash with --keep-index." 1>&2; exit 1)) && COMMIT=$(git rev-parse $1) && git commit --no-verify --fixup=$COMMIT && git rebase -i --autosquash $COMMIT~1' -'';
        uncommit = "reset --mixed HEAD~1";
        unpushed =
          "! git for-each-ref --format='%(refname:short)	%(push:trackshort)' refs/heads | grep -e '	$' -e '	.*>.*$' | cut -f 1";
      };

      extraConfig = {
        commit.verbose = true;
        diff = {
          ignoreSubmodules = "all";
          mnemonicPrefix = true;
        };
        github.user = "Sean1708";
        gitlab.user = "seamsay";
        init.defaultBranch = "main";
        merge.ff = false;
        pull.ff = "only";
        push.default = "upstream";
        rebase.autosquash = true;
        rerere.enabled = true;
      };

      # TODO: Can we have a derivation that builds this out of the github repo (https://github.com/github/gitignore)?
      ignores = let
        getSectionTitle = level: section: (repeat "#" level) + " " + section;
        prependSectionToPatterns = level: section: patterns: [
          (getSectionTitle level section)
          patterns
        ];
        repeat = string: n: lib.concatStrings (builtins.genList (_: string) n);
        makeIgnores' = level: ignores:
          if builtins.isAttrs ignores then
            makeIgnores' (level + 1)
            (lib.mapAttrsToList (prependSectionToPatterns level) ignores)
          else if builtins.isList ignores then
            map (makeIgnores' level) ignores
          else if builtins.isString ignores then
            ignores
          else
            makeIgnores' (toString ignores);
        makeIgnores = ignores: lib.flatten (makeIgnores' 1 ignores);
      in makeIgnores {
        dirEnv = ".direnv";
        linux = [ ".directory" ".Trash-*" ];
        vim = [
          "[._]*.s[a-w][a-z]"
          "[._]s[a-w][a-z]"
          "*.un~"
          "Session.vim"
          ".netrwhist"
          "*~"
        ];
        visualStudioCode = ".vscode";
      };
      userEmail = "srm.1708@gmail.com";
      userName = "Sean Marshallsay";
    };
  };
}
