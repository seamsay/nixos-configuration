{ config, lib, pkgs, ... }: {
  environment.systemPackages = lib.singleton pkgs.trash-cli;

  # TODO: Is creating a different service for each user the best way to do this?
  systemd = let
    # TODO: Is there a library function that already works like this?
    genAttrs' = names: createName: createValue:
      lib.mapAttrs' (name: value: lib.nameValuePair (createName name) value)
      (lib.genAttrs names createValue);
    getServiceName = user: "trash-cli-clean-" + user;
    # TODO: These should be shared.
    eachUser' = genAttrs';
    eachDefaultUser' = eachUser' config.myLib.defaultUsers;
  in {
    services = let
      defineService = user: {
        description = "Empty any trash items older than 7 days.";

        serviceConfig = {
          User = user;
          WorkingDirectory = config.users.users.${user}.home;
        };

        script = with pkgs;
          let
            trash-cli-cleaner = writeTextFile {
              name = "trash-cli-cleaner";
              executable = true;
              text = ''
                #!${bash}/bin/bash

                ${trash-cli}/bin/trash-empty 7
              '';
              checkPhase = "${shellcheck}/bin/shellcheck $out";
            };
          in "${trash-cli-cleaner}";
      };
    in eachDefaultUser' getServiceName defineService;

    timers = let
      defineTimer = user: {
        description = "Run the trash emptying service regularly.";

        wantedBy = [ "timers.target" ];

        timerConfig = {
          OnCalendar = "daily";
          Unit = (getServiceName user) + ".service";
        };
      };
    in eachDefaultUser' getServiceName defineTimer;
  };
}
