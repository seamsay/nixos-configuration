{ lib, pkgs, ... }: {
  # TODO: Run in the background, sending output to a temporary file.
  environment.systemPackages = lib.singleton (pkgs.writeTextFile rec {
    name = "notify";
    executable = true;
    destination = "/bin/${name}";
    text = ''
      #!${pkgs.bash}/bin/bash

      "$@"
      status="$?"

      if [ "$status" -eq 0 ]
      then
        result='Succeeded :D'
      else
        result='Failed :('
      fi

      if [ -n "$DISPLAY" ]
      then
        ${pkgs.libnotify}/bin/notify-send "$1" "$result"
      else
        echo -e "notify: $1: $result\a"
      fi
    '';
    checkPhase = "${pkgs.shellcheck}/bin/shellcheck $out/${destination}";
  });
}
