{ pkgs, ... }: {
  config.environment.systemPackages = with pkgs; [
    fd
    file
    jq
    julia-stable-bin
    kakoune
    nixfmt
    ripgrep
    tectonic
    tree
    xsv
  ];

  imports = [ ./git.nix ./notify.nix ./trash-cli.nix ];
}
