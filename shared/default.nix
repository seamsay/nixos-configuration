{ config, lib, pkgs, ... }: {
  options = {
    myConflicting = lib.mkOption {
      description = "Configuration for handling conflicting modules.";
      type = lib.types.submodule {
        options.graphical_headless = lib.mkOption {
          type = lib.types.submodule {
            options = {
              conflicts = lib.mkOption {
                description = "Which packages conflict with eachother?";
                type = lib.types.listOf (lib.types.submodule {
                  options = {
                    graphical = lib.mkOption {
                      description =
                        "The package that should be used on graphical environments.";
                      type = lib.types.package;
                    };
                    headless = lib.mkOption {
                      description =
                        "The package that should be used on headless environments.";
                      type = lib.types.package;
                    };
                  };
                });
              };
              which = lib.mkOption {
                description = "Is this machine graphical or headless?";
                # HINT: This is how we ensure conflicting modules aren't loaded.
                #       The `headless` module sets this to `"headless"` and the `graphical` module sets this to `"graphical"`.
                #       Since enums can't be merged the build will fail if both modules are included.
                # TODO: Is there a better way to do this?
                type = lib.types.enum [ "graphical" "headless" ];
              };
            };
          };
        };
      };
    };
    # TODO: Is this the right way to share functions among modules? We can now use the `extraArgs` argument of `flake-utils-plus`.
    myLib = lib.mkOption {
      description = "Functions and data to be shared across all modules.";
      type = lib.types.submodule {
        options = {
          defaultUsers = lib.mkOption {
            description = "The default interactive users.";
            type = lib.types.listOf lib.types.string;
          };
          eachDefaultUser = lib.mkOption {
            description = "Apply `eachUser` to the `defaultUsers` list.";
            type = lib.types.uniq lib.types.anything;
          };
          eachUser = lib.mkOption {
            description = ''
              Generate an attrset with each of the listed users as a key and the given attrset as the value for all of them.

                  [ users ] -> { options } -> { user = options }

              Specifically

                  eachUser [ "sean" "root" ] { an = ./option; }

              would generate

                  {
                    sean = { an = ./option; };
                    root = { an = ./option; };
                  }
            '';
            type = lib.types.uniq lib.types.anything;
          };
        };
      };
    };
  };

  config = {
    myConflicting.graphical_headless.conflicts = [
      {
        graphical = pkgs.mkvtoolnix;
        headless = pkgs.mkvtoolnix-cli;
      }
      {
        graphical = pkgs.transmission-gtk;
        headless = pkgs.transmission;
      }
    ];

    # TODO: Use `flake-utils-plus`.
    myLib = {
      defaultUsers = [ "sean" "root" ];
      eachDefaultUser = config.myLib.eachUser config.myLib.defaultUsers;
      eachUser = users: options: lib.genAttrs users (_: options);
    };

    # TODO: Set up music pause on headphone remove.
    # TODO: Set up bluetooth.

    system.stateVersion = "21.05";

    boot.cleanTmpDir = true;

    environment = {
      loginShellInit = ''
        # `setleds` will fail if used in a graphical environment.
        if [ -z "$DISPLAY" ]
        then
          setleds +num
        fi
      '';
      sessionVariables = {
        EDITOR = "${pkgs.kakoune}/bin/kak";
        TERMINAL = "${pkgs.konsole}/bin/konsole";
      };
    };

    home-manager = {
      sharedModules = lib.singleton {
        home = {
          enableNixpkgsReleaseCheck = true;
          keyboard = null;
          stateVersion = "21.11";
        };
        programs = {
          starship = {
            enable = true;
            settings = {
              directory.fish_style_pwd_dir_length = 1;
              git_metrics.disabled = false;
              shlvl.disabled = false;
              status.disabled = false;
            };
          };
        };

        xdg = {
          configFile = {
            "nushell/config.nu".source = ./config.nu;
            "nushell/env.nu".source = ./env.nu;
          };

          enable = true;
          mime.enable = true;
          mimeApps = let
            invertApplicationToSchemes = application: schemes:
              builtins.foldl'
              (accumulator: scheme: accumulator // { ${scheme} = application; })
              { } schemes;
            invertApplicationsToAppSchemes = applicationsToAppSchemes:
              lib.mapAttrsToList invertApplicationToSchemes
              applicationsToAppSchemes;
            concatSchemesToApplications = schemesToApplications:
              lib.zipAttrsWith (_: applications:
                assert builtins.length applications == 1;
                builtins.head applications) schemesToApplications;
            convertApplicationToDesktopFile = schemesToApplications:
              lib.mapAttrs (_: application: application + ".desktop")
              schemesToApplications;
            convert = applicationsToAppSchemes:
              convertApplicationToDesktopFile (concatSchemesToApplications
                (invertApplicationsToAppSchemes applicationsToAppSchemes));
          in {
            enable = true;
            associations = {
              added = convert {
                "org.bunkus.mkvtoolnix-gui.desktop" =
                  [ "video/mp4" "video/x-matroska" ];
                "vlc" = [ "application/x-partial-download" ];
              };
              removed = { "inode/directory" = "code.desktop"; };
            };
            defaultApplications = convert {
              "firefox" = [
                "application/pdf"
                "application/x-extension-htm"
                "application/x-extension-html"
                "application/x-extension-shtml"
                "application/x-extension-xht"
                "application/x-extension-xhtml"
                "application/xhtml+xml"
                "text/html"
                "x-scheme-handler/chrome"
                "x-scheme-handler/ftp"
                "x-scheme-handler/http"
                "x-scheme-handler/https"
              ];
              "vlc" = [ "video/mp4" "video/x-matroska" ];
              "userapp-transmission-gtk-88VJ00" = [ "x-scheme-handler/magnet" ];
            };
          };
        };
      };
      useGlobalPkgs = true;
      useUserPackages = true;

      users = config.myLib.eachDefaultUser { };
    };

    i18n.defaultLocale = "en_GB.UTF-8";

    networking = {
      hosts = { "157.245.14.251" = [ "srm-no-digitalocean" "digitalocean" ]; };
      networkmanager.enable = true;
    };

    nix = {
      extraOptions = ''
        experimental-features = nix-command flakes
      '';
      gc.automatic = true;
      optimise.automatic = true;
      package = pkgs.nixUnstable;
      settings.auto-optimise-store = true;
    };

    nixpkgs.config.allowUnfree = true;

    programs = {
      bash.interactiveShellInit = ''
        if [ -z "$NO_NU" ]
        then
          exec ${pkgs.nushell}/bin/nu
        fi
      '';
      nm-applet.enable = true;
      ssh = {
        extraConfig = ''
          AddKeysToAgent yes
        '';
        startAgent = true;
      };
    };

    services = {
      locate.enable = true;
      openssh = {
        enable = true;
        permitRootLogin = "no";
      };
    };

    system.autoUpgrade.enable = true;

    time.timeZone = "Europe/London";

    users.users.sean = {
      description = "Sean Marshallsay";
      extraGroups = [ "networkmanager" "wheel" ];
      isNormalUser = true;
      initialHashedPassword =
        "$6$mQ8oTq4hyGA5T7Sp$bZp/HDTB6MzAKhJzB.s9kCmMiqLFdZWfh8O.s9mN/T0TVpk2CqTjJJ53lCmcsHgPnQjYbj1caDd3LXEZwhbUy1";
    };
  };

  imports = [ ./packages ];
}
