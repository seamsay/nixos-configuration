# TODO: All keybindings should be handled by the window managers.
{ config, lib, pkgs, ... }:
let
  keyBindings = {
    XF86AudioMute = "pactl set-sink-mute 0 toggle";
    XF86AudioLowerVolume = "pactl set-sink-volume 0 -5%";
    XF86AudioRaiseVolume = "pactl set-sink-volume 0 +5%";
    XF86AudioMicMute = "pactl set-source-mute 1 toggle";
    XF86MonBrightnessDown = "brightnessctl set 5%-";
    XF86MonBrightnessUp = "brightnessctl set 5%+";

    Alt = {
      XF86AudioMute = "playerctl play-pause";
      XF86AudioLowerVolume = "playerctl previous";
      XF86AudioRaiseVolume = "playerctl next";
    };

    # XF86AudioMute
    # XF86AudioLowerVolume
    # XF86AudioRaiseVolume
    # XF86AudioMicMute
    # XF86MonBrightnessDown
    # XF86MonBrightnessUp
    # XF86Display
    # XF86WLAN (OS)
    # XF86Tools
    # XF86Bluetooth (OS)
    # XF86Keyboard
    # XF86Favorites
    # XF86Sleep (OS)
    # XF86WakeUp (Function Key)
    # Print
    # XF86Calculator
    # Shift_[L/R]
    # Ctrl_[L/R]
    # Alt_L
    # ISO_Level3_Shift (Alt Gr)
  };

  formatKeyAttrsPair = metas: attrs:
    (builtins.concatStringsSep "\n\n"
      (lib.mapAttrsToList (key: value: formatKeyCmdPair metas key value)
        attrs));

  formatKeyCmdPair = metas: key: cmd:
    if builtins.isAttrs cmd then
      formatKeyAttrsPair (metas ++ [ key ]) cmd
    else ''
      "${cmd}"
       ${builtins.concatStringsSep " + " (metas ++ [ key ])}
    '';

  xbindkeysFile =
    pkgs.writeText "xbindkeysrc" (formatKeyAttrsPair [ ] keyBindings);
in {
  environment.systemPackages = with pkgs; [ brightnessctl playerctl ];

  hardware.pulseaudio.enable = true;

  services.xserver = {
    displayManager.sessionCommands =
      "${pkgs.xbindkeys}/bin/xbindkeys --file ${xbindkeysFile}";
    xkbOptions = builtins.concatStringsSep ", " [
      "altwin:prtsc_rwin"
      "caps:swapescape"
      "terminate:ctrl_alt_bksp"
    ];
  };

  # TODO: Add all users to these groups.
  users.users =
    config.myLib.eachDefaultUser { extraGroups = [ "input" "video" ]; };
}
