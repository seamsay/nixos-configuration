{ config, lib, pkgs, ... }: {
  config = {
    myConflicting.graphical_headless.which = "graphical";

    environment.systemPackages = with pkgs;
      [
        fira-code
        fira-mono
        firefox
        jupyter
        konsole
        libnotify
        spotify
        xfce.thunar
        xsel
        vlc
        zotero
      ] ++ lib.catAttrs "graphical"
      config.myConflicting.graphical_headless.conflicts;
  };

  imports = [ ./dunst.nix ./rofi.nix ./udiskie.nix ./vscode ];
}
