{ lib, pkgs, ... }: {
  home-manager.sharedModules = lib.singleton {
    programs = {
      direnv = {
        enable = true;
        nix-direnv.enable = true;
      };
      vscode = {
        enable = true;
        mutableExtensionsDir = false;
        extensions = with pkgs.vscode-extensions;
          [
            asciidoctor.asciidoctor-vscode
            christian-kohler.path-intellisense
            bierner.emojisense
            eamodio.gitlens
            grapecity.gc-excelviewer
            jnoortheen.nix-ide
            mhutchie.git-graph
            ms-pyright.pyright
            ms-python.vscode-pylance
            ms-python.python
            ms-toolsai.jupyter
            ms-toolsai.jupyter-renderers
            ms-vscode.cpptools
            rust-lang.rust-analyzer
            streetsidesoftware.code-spell-checker
            tamasfe.even-better-toml
          ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
            # TODO: Make a flake that provides all of these.
            {
              name = "dance";
              publisher = "gregoire";
              version = "0.5.11";
              sha256 = "zH/gvrnQRPMSY9DUB61xyXm4wmOnD6s1+6NqNfJa+HY=";
            }
            {
              name = "direnv";
              publisher = "mkhl";
              version = "0.6.1";
              sha256 = "5/Tqpn/7byl+z2ATflgKV1+rhdqj+XMEZNbGwDmGwLQ=";
            }
            {
              name = "language-julia";
              publisher = "julialang";
              version = "1.6.24";
              sha256 = "q0iNNOCIlV5eIsgtvjVPQXGUhjZCes0u7ZGUvX1YkCQ=";
            }
            {
              name = "vscode-js-profile-flame";
              publisher = "ms-vscode";
              version = "1.0.4";
              sha256 = "06tMdExfHWzNjVhUl6vmiJlcSy+eyRhj212Ad3B2mlM=";
            }
            {
              name = "vscode-theme-onelight";
              publisher = "akamud";
              version = "2.2.3";
              sha256 = "H86Tk/YBJVqzX6Icx0YpGCGi3/u7bVe2m2ZRs/U57dc=";
            }
          ];
        keybindings = [
          {
            key = "ctrl+tab";
            command = "workbench.action.nextEditorInGroup";
            when = "";
          }
          {
            key = "ctrl+shift+tab";
            command = "workbench.action.previousEditorInGroup";
            when = "";
          }
          {
            key = "shift+up";
            command = "workbench.action.focusAboveGroup";
            when = "editorTextFocus && dance.mode == 'normal'";
          }
          {
            key = "shift+down";
            command = "workbench.action.focusBelowGroup";
            when = "editorTextFocus && dance.mode == 'normal'";
          }
          {
            key = "shift+left";
            command = "workbench.action.focusLeftGroup";
            when = "editorTextFocus && dance.mode == 'normal'";
          }
          {
            key = "shift+right";
            command = "workbench.action.focusRightGroup";
            when = "editorTextFocus && dance.mode == 'normal'";
          }
          {
            key = "ctrl+f";
            command = "-dance.select.vertically";
            when = "editorTextFocus && dance.mode == 'normal'";
          }
          {
            key = "ctrl+shift+f";
            command = "-dance.select.vertically";
            when = "editorTextFocus && dance.mode == 'normal'";
          }
          {
            key = "ctrl+b";
            command = "-dance.select.vertically";
            when = "editorTextFocus && dance.mode == 'normal'";
          }
          {
            key = "ctrl+shift+b";
            command = "-dance.select.vertically";
            when = "editorTextFocus && dance.mode == 'normal'";
          }
        ];
        userSettings = let
          dictionaries = [
            ./dictionaries/asciidoc.txt
            ./dictionaries/british.txt
            ./dictionaries/julia.txt
            ./dictionaries/nix.txt
            ./dictionaries/rust.txt
            ./dictionaries/science.txt
          ];
          dictionaryName = path: "my-${baseNameOf path}";
        in {
          "[asciidoc]" = {
            "editor.detectIndentation" = false;
            "editor.quickSuggestions" = {
              "comments" = "off";
              "strings" = "off";
              "other" = "off";
            };
          };
          "asciidoc.use_kroki" = true;
          "cSpell.dictionaryDefinitions" = map (path: {
            name = dictionaryName path;
            inherit path;
          }) dictionaries;
          "cSpell.dictionaries" = map dictionaryName dictionaries;
          "cSpell.enableFiletypes" = [
            "Log"
            "bat"
            "bibtex"
            "bibtex-style"
            "clojure"
            "code-text-binary"
            "coffeescript"
            "cpp_embedded_latex"
            "csv"
            "cuda-cpp"
            "dart"
            "diff"
            "dockercompose"
            "dockerfile"
            "doctex"
            "editorconfig"
            "excel"
            "fsharp"
            "git-rebase"
            "groovy"
            "hlsl"
            "ignore"
            "ini"
            "jinja"
            "jlweave"
            "jsx-tags"
            "julia"
            "juliamarkdown"
            "latex-expl3"
            "log"
            "lua"
            "makefile"
            "markdown-math"
            "markdown_latex_combined"
            "nix"
            "objective-c"
            "objective-cpp"
            "perl"
            "perl6"
            "pip-requirements"
            "powershell"
            "properties"
            "r"
            "ra_syntax_tree"
            "raw"
            "razor"
            "rsweave"
            "ruby"
            "scminput"
            "search-result"
            "shaderlab"
            "shellscript"
            "sql"
            "tex"
            "toml"
            "tsv"
            "vb"
            "xml"
            "xsl"
          ];
          "cSpell.language" = "en-GB";
          "cSpell.userWords" = [ "Marshallsay" ];
          "editor.acceptSuggestionOnEnter" = "smart";
          "editor.fontFamily" = "'Fira Code'";
          "editor.fontLigatures" = true;
          "editor.fontSize" = 12;
          "editor.inlayHints.enabled" = "offUnlessPressed";
          "editor.wordWrap" = "on";
          "editor.wrappingIndent" = "deepIndent";
          "explorer.openEditors.visible" = 0;
          "extensions.autoUpdate" = false;
          "extensions.ignoreRecommendations" = true;
          "files.autoSave" = "afterDelay";
          "files.trimFinalNewlines" = true;
          "files.trimTrailingWhitespace" = true;
          "git.autofetch" = true;
          "git.confirmSync" = false;
          "gitlens.codeLens.enabled" = false;
          "julia.enableCrashReporter" = true;
          "julia.enableTelemetry" = true;
          "julia.symbolCacheDownload" = true;
          "keyboard.dispatch" = "keyCode";
          "nix.formatterPath" = "${pkgs.nixfmt}/bin/nixfmt";
          "nix.serverPath" = "${pkgs.rnix-lsp}/bin/rnix-lsp";
          "rust-analyzer.checkOnSave.command" = "clippy";
          "terminal.integrated.commandsToSkipShell" =
            [ "language-julia.interrupt" ];
          "window.restoreWindows" = "none";
          "window.titleBarStyle" = "custom";
          "workbench.colorTheme" = "Atom One Light";
          "workbench.editor.enablePreview" = false;
          "workbench.preferredLightColorTheme" = "Atom One Light";
        };
      };
    };
  };
}
