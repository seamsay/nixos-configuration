{ lib, pkgs, ... }: {
  environment.systemPackages = lib.singleton pkgs.rofi;

  home-manager.sharedModules = lib.singleton { programs.rofi.enable = true; };
}
