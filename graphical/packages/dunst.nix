{ lib, ... }: {
  home-manager.sharedModules = lib.singleton {
    services.dunst = {
      enable = true;
      settings = {
        global = {
          follow = "keyboard";
          geometry = "300x5-25+25";
          indicate_hidden = true;
          separator_height = 2;
          padding = 8;
          horizontal_padding = 8;
          frame_width = 3;
          frame_color = "#aaaaaa";
          seperator_color = "frame";
          sort = true;
          idle_threshold = 120;
          font = "FiraMono 8";
          markup = "full";
          format = "<b>%a: %s</b>\\n%b";
          word_wrap = true;
          stack_duplicates = true;
          show_indicators = true;
        };

        shortcuts = {
          # HINT: `mod1` is `alt`.
          close = "mod1+space";
          close_all = "mod1+shift+space";
          context = "mod1+period";
          history = "mod1+grave";
        };

        urgency_low = {
          background = "#222222";
          foreground = "#888888";
          timeout = 10;
        };
        urgency_normal = {
          background = "#285577";
          foreground = "#ffffff";
          timeout = 10;
        };
        urgency_critical = {
          background = "#900000";
          foreground = "#ffffff";
          frame_color = "#ff0000";
          timeout = 0;
        };
      };
    };
  };
}
