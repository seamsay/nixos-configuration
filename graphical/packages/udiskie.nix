{ lib, pkgs, ... }: {
  environment.systemPackages = lib.singleton pkgs.udiskie;

  home-manager.sharedModules =
    lib.singleton { services.udiskie.enable = true; };
}
