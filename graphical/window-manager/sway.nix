{ lib, pkgs, ... }: {
  home-manager.sharedModules = lib.singleton ({ config, pkgs, ... }: {
    wayland.windowManager.sway = {
      enable = false;

      config = let
        # Global keybindings.
        #  Available keybindings:
        #   XF86AudioMute
        #   XF86AudioLowerVolume
        #   XF86AudioRaiseVolume
        #   XF86AudioMicMute
        #   XF86MonBrightnessDown
        #   XF86MonBrightnessUp
        #   XF86Display
        #   XF86WLAN (OS)
        #   XF86Tools
        #   XF86Bluetooth (OS)
        #   XF86Keyboard
        #   XF86Favorites
        #   XF86Sleep (OS)
        #   XF86WakeUp (Function Key)
        #   Print
        #   XF86Calculator
        #   Shift_[L/R]
        #   Ctrl_[L/R]
        #   Alt_L
        #   ISO_Level3_Shift (Alt Gr)
        # TODO: Use wob with these.
        globalKeybindings = let
          brightnessctl = "${pkgs.brightnessctl}/bin/brightnessctl";
          pactl = "${pkgs.pulseaudio}/bin/pactl";
          playerctl = "${pkgs.playerctl}/bin/playerctl";
        in {
          XF86AudioMute = "exec ${pactl} set-sink-mute 0 toggle";
          XF86AudioLowerVolume = "exec ${pactl} set-sink-volume 0 -5%";
          XF86AudioRaiseVolume = "exec ${pactl} set-sink-volume 0 +5%";
          XF86AudioMicMute = "exec ${pactl} set-source-mute 1 toggle";

          XF86MonBrightnessDown = "exec ${brightnessctl} set 5%-";
          XF86MonBrightnessUp = "exec ${brightnessctl} set 5%+";

          # TODO: Allow this to be specified as `{ Alt = { XF...`.
          "Alt+XF86AudioMicMute" = "exec ${playerctl} play-pause";
          "Alt+XF86AudioLowerVolume" = "exec ${playerctl} previous";
          "Alt+XF86AudioRaiseVolume" = "exec ${playerctl} next";
        };
      in {
        bars = [{ fonts = config.wayland.windowManager.sway.config.fonts; }];

        floating.criteria = config.windowManager.floating.criteria;

        focus.followMouse = config.windowManager.focus.followMouse;

        fonts = config.windowManager.fonts;

        gaps = {
          inner = 6;
          outer = 3;
        };

        input = {
          "type:keyboard" = {
            xkb_layout = "gb";
            xkb_numlock = "enabled";
            xkb_options = builtins.concatStringsSep "," [
              "altwin:prtsc_rwin"
              "caps:swapescape"
              "terminate:ctrl_alt_bksp"
            ];
          };
          "type:touchpad" = {
            click_method = "clickfinger";
            drag = "disabled";
            natural_scroll = "enabled";
            scroll_method = "two_finger";
            tap = "disabled";
          };
        };

        keybindings = (config.windowManager.makeKeybindings
          config.wayland.windowManager.sway.config.menu
          config.wayland.windowManager.sway.config.terminal)
          // globalKeybindings;

        menu = "exec ${pkgs.ulauncher}/bin/ulauncher";

        modes =
          builtins.mapAttrs (_mode: commands: commands // globalKeybindings)
          (config.windowManager.makeModes
            config.wayland.windowManager.sway.config.menu
            config.wayland.windowManager.sway.config.terminal);

        modifier = config.windowManager.modifier;

        output."*".background =
          "${pkgs.nixos-artwork.wallpapers.simple-dark-gray.gnomeFilePath} fill";

        startup = config.windowManager.startup ++ [
          # Needed so that i3 starts on workspace 1, rather than 10.
          # TODO: Is there a better way? Fundamentally this seems to happen because attrsets are stored with keys ordered, so the keybinding for zero appears first.
          { command = "swaymsg workspace 1"; }
        ];

        terminal = "exec ${pkgs.alacritty}/bin/alacritty";

        window.commands = config.windowManager.window.commands;

        workspaceLayout = config.windowManager.workspaceLayout;
      };

      package = null;
    };
  });

  programs.sway = {
    enable = false;
    extraPackages = [ pkgs.swayidle ];
    wrapperFeatures.gtk = true;
  };
}

