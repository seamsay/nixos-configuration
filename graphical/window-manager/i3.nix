{ lib, ... }: {
  home-manager.sharedModules = lib.singleton ({ config, pkgs, ... }: {
    xsession.windowManager.i3 = {
      enable = true;
      config = let
        menu = ''
          exec "${pkgs.rofi}/bin/rofi -matching fuzzy -show drun -modi 'drun,run'"'';
        terminal = "exec ${pkgs.rofi}/bin/rofi-sensible-terminal";
      in {
        bars = [{
          colors = rec {
            activeWorkspace = {
              background = "#EAEAEB";
              border = "#DBDBDC";
              text = "#121417";
            };
            background = "#FAFAFA";
            bindingMode = {
              background = "#FAFAFA";
              border = "#526FFF";
              text = "#121417";
            };
            focusedWorkspace = {
              background = "#FAFAFA";
              border = "#DBDBDC";
              text = "#121417";
            };
            inactiveWorkspace = {
              background = "#EAEAEB";
              border = "#DBDBDC";
              text = "#626772";
            };
            separator = "#DBDBDC";
            statusline = "#121417";
            urgentWorkspace = {
              background = "#EAEAEB";
              border = "#CA1243";
              text = "#626772";
            };
          };
          statusCommand = "${pkgs.i3status}/bin/i3status";
        }];

        colors = {
          background = "#FAFAFA";
          focused = {
            background = "#FAFAFA";
            border = "#DBDBDC";
            childBorder = "#DBDBDC";
            indicator = "#9D9D9F";
            text = "#121417";
          };
          focusedInactive = {
            background = "#EAEAEB";
            border = "#DBDBDC";
            childBorder = "#DBDBDC";
            indicator = "#9D9D9F";
            text = "#121417";
          };
          placeholder = {
            background = "#FAFAFA";
            border = "#DBDBDC";
            childBorder = "#DBDBDC";
            indicator = "#9D9D9F";
            text = "#121417";
          };
          unfocused = {
            background = "#EAEAEB";
            border = "#DBDBDC";
            childBorder = "#DBDBDC";
            indicator = "#9D9D9F";
            text = "#626772";
          };
          urgent = {
            background = "#EAEAEB";
            border = "#CA1243";
            childBorder = "#CA1243";
            indicator = "#9D9D9F";
            text = "#626772";
          };
        };

        floating.criteria = config.windowManager.floating.criteria;

        focus.followMouse = config.windowManager.focus.followMouse;

        fonts = config.windowManager.fonts;

        keybindings = config.windowManager.makeKeybindings menu terminal;

        modes = config.windowManager.makeModes menu terminal;

        modifier = config.windowManager.modifier;

        startup = config.windowManager.startup ++ [
          {
            command = "${pkgs.numlockx}/bin/numlockx on";
            notification = false;
          }
          {
            command =
              "${pkgs.feh}/bin/feh --no-fehbg --bg-scale ${pkgs.nixos-artwork.wallpapers.simple-light-gray.gnomeFilePath}";
            notification = false;
          }
          # Needed so that i3 starts on workspace 1, rather than 10.
          # TODO: Is there a better way to do this? Maybe `defaultWorkspace`?
          {
            command = "i3-msg workspace 1";
            notification = false;
          }
        ];

        window.commands = config.windowManager.window.commands;

        workspaceLayout = config.windowManager.workspaceLayout;
      };

      extraConfig = ''
        no_focus [title="^i3-treevisual - Movement - "]
      '';
    };

    programs.i3status = {
      enable = true;
      general = {
        colors = true;
        color_good = "#4CC263";
        color_degraded = "#F2A60D";
        color_bad = "#E45649";
      };
    };
  });

  services.xserver.windowManager.i3.enable = true;
}
