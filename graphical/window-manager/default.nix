{ lib, i3-keyvisual, i3-treeversal, i3-treevisual, ... }: {
  home-manager.sharedModules = lib.singleton ({ lib, pkgs, ... }: {
    options.windowManager = let mkOption = lib.mkOption;
    in with lib.types;
    let
      fontOptions = submodule {
        options = {
          names = mkOption { type = listOf str; };
          size = mkOption { type = float; };
        };
      };
    in mkOption {
      description = "Shared configuration for i3 and sway.";
      type = submodule {
        options = {
          floating = mkOption {
            type = submodule {
              options.criteria = mkOption { type = listOf attrs; };
            };
          };

          focus = mkOption {
            type =
              submodule { options.followMouse = mkOption { type = bool; }; };
          };

          fonts = mkOption { type = fontOptions; };

          keys = mkOption {
            description = "Keys which are used in many places.";
            type = attrsOf (either str (listOf str));
          };

          makeKeybindings = mkOption {
            description =
              "Function to create the global keybindings given the menu and terminal being used.";
            type = anything;
          };

          makeModes = mkOption {
            description =
              "Function to create the modal keybindings given the menu and terminal being used.";
            type = anything;
          };

          modeNames = mkOption {
            description = "Display names for the various modes.";
            type = attrsOf str;
          };

          modifier = mkOption { type = str; };

          secondaryModifier = mkOption {
            description =
              "Secondary modifier key to be used in chorded keybindings.";
            type = str;
          };

          startup = mkOption {
            type = listOf (submodule {
              options = {
                command = mkOption { type = str; };
                notification = mkOption { type = bool; };
              };
            });
          };

          window = mkOption {
            type = submodule {
              options.commands = mkOption {
                type = listOf (submodule {
                  options = {
                    command = mkOption { type = str; };
                    criteria = mkOption { type = attrsOf (either bool str); };
                  };
                });
              };
            };
          };

          workspaceLayout =
            mkOption { type = enum [ "split" "stacked" "tabbed" ]; };
        };
      };
    };

    config = {
      # TODO: The fonts installed into `environment.systemPackages` don't seem to work. :(
      home.packages = with pkgs; [ fira-code fira-mono ];

      windowManager = let
        makeWorkspaceKeybindings = let
          map10To0 = n: lib.trivial.mod n 10;

          makeWorkspaceKeybinding = makeKey: makeCommand: workspace:
            let
              key = makeKey (toString (map10To0 workspace));
              command = makeCommand (toString (workspace));
            in lib.attrsets.nameValuePair key command;
        in makeKey: makeCommand:
        let
          workspaces = builtins.genList (n: n + 1) 10;

          workspaceKeybindings =
            map (makeWorkspaceKeybinding makeKey makeCommand) workspaces;
        in builtins.listToAttrs workspaceKeybindings;
      in rec {
        floating.criteria =
          [ { class = "^i3-keyvisual$"; } { class = "^i3-treevisual$"; } ];

        focus.followMouse = false;

        fonts = {
          # TODO: Add these to packages automatically.
          names = [ "FiraMono" ];
          size = 8.0;
        };

        keys = {
          exit = [ "BackSpace" "Escape" ];
          # NOTE: For some reason i3 expects `space` to not be capitalised...
          menu = "space";
          terminal = "Return";
        };

        makeKeybindings = menu: terminal:
          let
            prefixModifier = key: "${modifier}+${key}";

            prefixModifierToKey = key: command:
              lib.attrsets.nameValuePair (prefixModifier key) command;
          in lib.attrsets.mapAttrs' prefixModifierToKey {
            ${keys.menu} = menu;
            ${keys.terminal} = terminal;

            q = "kill";

            h = "focus left";
            j = "focus down";
            k = "focus up";
            l = "focus right";

            "${secondaryModifier}+j" = "focus child";
            "${secondaryModifier}+k" = "focus parent";

            "${secondaryModifier}+h" = "workspace prev_on_output";
            "${secondaryModifier}+l" = "workspace next_on_output";

            f = "fullscreen toggle";
            Tab = "focus mode_toggle";

            b = ''[class="^i3-keyvisual$"] scratchpad show; focus tiling'';
            t = ''[class="^i3-treevisual$"] scratchpad show; focus tiling'';

            a = ''mode "${modeNames.applications}"'';
            c = ''mode "${modeNames.container}"'';
            # TODO: Set `focus_wrapping force` when entering, then unset it when leaving.
            m = ''
              [class="^i3-treevisual$"] scratchpad show; focus tiling; mode "${modeNames.movement}"'';
            s = ''mode "${modeNames.system}"'';
          } // (makeWorkspaceKeybindings prefixModifier
            (workspace: "workspace ${workspace}"));

        makeModes = let
          makeExitKeybinding = key:
            lib.attrsets.nameValuePair key ''mode "default"'';

          exitKeybindings =
            builtins.listToAttrs (map makeExitKeybinding keys.exit);

          appendExitCommand = command: ''${command}, mode "default"'';

          makeKeybindings =
            keybindingsThatRemainInMode: keybindingsThatLeaveMode:
            keybindingsThatRemainInMode
            // builtins.mapAttrs (_: appendExitCommand)
            keybindingsThatLeaveMode;

          makeKeybindingsWithExit =
            keybindingsThatRemainInMode: keybindingsThatLeaveMode:
            (makeKeybindings keybindingsThatRemainInMode
              keybindingsThatLeaveMode) // exitKeybindings;

          moveToWorkspaceKeybindings = makeWorkspaceKeybindings lib.trivial.id
            (workspace: "move container to workspace ${workspace}");

          makeHideTreeVisualKeybinding = key:
            lib.attrsets.nameValuePair key
            ''[class="^i3-treevisual$"] scratchpad show'';

          hideTreeVisualKeybindings =
            builtins.listToAttrs (map makeHideTreeVisualKeybinding keys.exit);
        in menu: terminal: {
          ${modeNames.applications} = makeKeybindingsWithExit {
            h = "split horizontal";
            v = "split vertical";
          } {
            b = "exec ${pkgs.firefox}/bin/firefox";
            e = "exec ${pkgs.vscode}/bin/code";
            m = "exec ${pkgs.spotify}/bin/spotify";

            ${keys.menu} = menu;
            ${keys.terminal} = terminal;
          };

          ${modeNames.container} = makeKeybindingsWithExit {
            h = "resize shrink width 10 px or 10 ppt";
            j = "resize shrink height 10 px or 10 ppt";
            k = "resize grow height 10 px or 10 ppt";
            l = "resize grow width 10 px or 10 ppt";
          } {
            f = "floating toggle";
            p = "layout toggle split";
            s = "layout stacking";
            t = "layout tabbed";
          };

          ${modeNames.movement} = makeKeybindings {
            h = "exec ${i3-treeversal} move left";
            j = "exec ${i3-treeversal} move child";
            k = "exec ${i3-treeversal} move parent";
            l = "exec ${i3-treeversal} move right";
            n = "exec ${i3-treeversal} move child-left";
            m = "exec ${i3-treeversal} move child-right";
            "${modifier}+h" = "move left";
            "${modifier}+j" = "move down";
            "${modifier}+k" = "move up";
            "${modifier}+l" = "move right";
            "${secondaryModifier}+h" = "focus prev sibling";
            "${secondaryModifier}+j" = "focus child";
            "${secondaryModifier}+k" = "focus parent";
            "${secondaryModifier}+l" = "focus next sibling";
            "${secondaryModifier}+n" = "focus prev sibling; focus child";
            "${secondaryModifier}+m" = "focus prev sibling; focus child";
            "${modifier}+${secondaryModifier}+h" = "focus left";
            "${modifier}+${secondaryModifier}+j" = "focus down";
            "${modifier}+${secondaryModifier}+k" = "focus up";
            "${modifier}+${secondaryModifier}+l" = "focus right";
          } (moveToWorkspaceKeybindings // hideTreeVisualKeybindings);

          ${modeNames.system} = makeKeybindingsWithExit {
            b = "exec --no-startup-id systemctl reboot";
            c = "reload";
            q = "exit";
            r = "restart";
            s = "exec --no-startup-id systemctl poweroff";
          } { l = "exec --no-startup-id loginctl lock-session"; };
        };

        modeNames = {
          applications = "Applications";
          container = "Container";
          movement = "Move";
          system = "System";
        };

        modifier = "Mod4";

        secondaryModifier = "Shift";

        startup = [
          {
            command = i3-keyvisual;
            notification = false;
          }
          {
            command = i3-treevisual;
            notification = false;
          }
        ];

        window.commands = [
          {
            command =
              "resize set width 70 ppt height 70 ppt, move position center, sticky enable, move scratchpad";
            criteria = { class = "^i3-(tree|key)visual$"; };
          }
          {
            command = "title_window_icon yes";
            criteria = { all = true; };
          }
        ];

        workspaceLayout = "tabbed";
      };
    };
  });

  imports = [ ./i3.nix ./sway.nix ];
}
