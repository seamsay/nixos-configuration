{ config, lib, pkgs, ... }: {
  config = {
    # TODO: XServer config should go in i3 module.
    console.useXkbConfig = true;

    home-manager.sharedModules = lib.singleton {
      fonts.fontconfig.enable = true;

      systemd.user.services.polkit-gui = {
        Unit = {
          Description = "Polkit GUI Authenticator";
          After = [ "graphical-session-pre.target" ];
          PartOf = [ "graphical-sess-pre.target" ];
        };
        Install.WantedBy = [ "graphical-session.target" ];
        Service.ExecStart =
          "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
      };

      xsession.enable = true;
    };

    services = {
      gnome.gnome-keyring.enable = true;
      xserver = {
        displayManager = {
          lightdm.enable = true;
          sessionCommands = "${pkgs.lightlocker}/bin/light-locker &";
        };
        enable = true;
        layout = "gb";
        libinput = {
          enable = true;
          touchpad = {
            clickMethod = "clickfinger";
            naturalScrolling = true;
            tapping = false;
          };
        };
      };
    };
  };

  imports = [ ./keybindings.nix ./packages ./window-manager ];
}
