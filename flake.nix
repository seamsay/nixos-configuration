{
  inputs = {
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # TODO: Lots of good stuff in the example of this repository, e.g. Nix configured Firefox.
    # TODO: AND https://github.com/gytis-ivaskevicius/nixfiles/blob/4bc0542bfb273af1fd8a108698e4caf12e650339/config/sway.nix#L19
    flake-utils-plus = {
      url = "github:gytis-ivaskevicius/flake-utils-plus/v1.3.1";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    i3-keyvisual = {
      url = "gitlab:seamsay/i3-keyvisual";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    i3-treeversal = {
      url = "gitlab:seamsay/i3-treeversal";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    i3-treevisual = {
      url = "gitlab:seamsay/i3-treevisual";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = inputs@{ self, home-manager, flake-utils-plus, i3-keyvisual
    , i3-treeversal, i3-treevisual, nixpkgs }:
    flake-utils-plus.lib.mkFlake {
      inherit self inputs;

      channelsConfig = { allowUnfree = true; };

      hostDefaults.modules =
        [ home-manager.nixosModules.home-manager ./shared ];

      hosts.srm-no-t570 = rec {
        system = "x86_64-linux";
        extraArgs = {
          i3-keyvisual = i3-keyvisual.defaultApp.${system}.program;
          i3-treeversal = i3-treeversal.defaultApp.${system}.program;
          i3-treevisual = i3-treevisual.defaultApp.${system}.program;
        };
        modules = [
          ./hardware/srm-no-t570.nix
          ./accessible.nix
          ./dual-boot.nix
          ./graphical
          ./laptop
        ];
      };
    };
}
