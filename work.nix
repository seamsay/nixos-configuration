{
  home-manager.users.artesia = { lib, pkgs, ... }: {
    home.packages = with pkgs; with rPackages; [ R renv slack teams ];
    programs.git.userEmail =
      lib.mkForce "sean.marshallsay@artesia-consulting.co.uk";
    xdg.mimeApps.defaultApplications."x-scheme-handler/msteams" =
      "teams.desktop";
  };
  myLib.defaultUsers = [ "artesia" ];
  users.users.artesia = {
    description = "Artesia - Sean Marshallsay";
    isNormalUser = true;
    passwordFile = "/secrets/artesia/password";
  };
}
