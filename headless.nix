{ config, lib, ... }: {
  myConflicting.graphical_headless.which = "headless";
  environment.systemPackages =
    lib.catAttrs "headless" config.myConflicting.graphical_headless.conflicts;
}
